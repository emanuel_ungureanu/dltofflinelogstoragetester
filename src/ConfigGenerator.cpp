/**
 * @file ConfigGenerator.cpp
 * @author Emanuel Ungureanu <emanuel.ungureanu@gmail.com>
 * @date 2018-10-29
 * 
 * @copyright Copyright (c) 2018 Emanuel Ungureanu. All rights reserved.
 * @license This project is released under the MIT License
 * 
 */
#include "ConfigGenerator.hpp"

#include <iostream>
#include <fstream>



ConfigGenerator::ConfigGenerator(const std::string &templatePath, const std::string &confPath, const std::string &workDir, const std::string &logstoragePath) :
    mPathTemplate(templatePath),
    mPathDltConf(confPath), 
    mWorkDir(workDir),
    mPathLogstorage(logstoragePath)
{
    mConfigArguments = 
    {
        "PersistanceStoragePath",
        "LoggingFilename",
        "ControlSocketPath",
        "OfflineLogstorageDirPath",
    };
}

void ConfigGenerator::generate()
{
    using namespace std;

    ifstream templateStream = ifstream(mPathTemplate, ios::binary);
    ofstream configFile = ofstream(mPathDltConf, ios::binary);

    if(templateStream.is_open() == false || configFile.is_open() == false)
    {
        cout << "Error opening files. Is template stream open: " << std::noboolalpha << templateStream.is_open();
        cout << ". Is config file open: " << std::noboolalpha << configFile.is_open() << endl;
        return;
    }

    while( getline( templateStream, mCurrentTemplateLine) )
    {
        if( isArgumentInLine(0) )  // PersistenceStoragePath
        {
            configFile << mConfigArguments[0] << " = " << mWorkDir;
        }
        else if( isArgumentInLine(1) ) // LoggingFilename
        {
            configFile << mConfigArguments[1] << " = " << mWorkDir << "/" << "dlt-test.log";
        }
        else if( isArgumentInLine(2) ) // ControlSocketPath
        {
            configFile << mConfigArguments[2] << " = " << mWorkDir << "/" << "dlt-ctrl-test.sock";
        }
        else if( isArgumentInLine(3) ) // OfflineLogstorageDirPath
        {
            configFile << mConfigArguments[3] << " = " << mPathLogstorage;
        }
        else 
        {
            configFile << mCurrentTemplateLine;
        }
        configFile << endl;
    }
}

bool ConfigGenerator::isArgumentInLine(size_t argumentIndex) const
{
    const std::string &argument = mConfigArguments[argumentIndex];
    return mCurrentTemplateLine.compare(0, argument.length(), argument) == 0;
}