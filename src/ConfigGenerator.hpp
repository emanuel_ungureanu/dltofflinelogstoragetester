/**
 * @file ConfigGenerator.hpp
 * @author Emanuel Ungureanu <emanuel.ungureanu@gmail.com>
 * @date 2018-10-28
 * 
 * @copyright Copyright (c) 2018 Emanuel Ungureanu. All rights reserved.
 * @license This project is released under the MIT License
 */
#pragma once

#include <string>
#include <vector>

class ConfigGenerator
{
    public:
        ConfigGenerator(const std::string &templatePath, const std::string &confPath, const std::string &workDir, const std::string &logstoragePath);

        void generate();

    private:
        bool isArgumentInLine(size_t argumentIndex) const;


    private:
        std::string mPathTemplate;
        std::string mPathDltConf;
        std::string mWorkDir;
        std::vector<std::string> mConfigArguments;
        std::string mCurrentTemplateLine;
        std::string mPathLogstorage;
};