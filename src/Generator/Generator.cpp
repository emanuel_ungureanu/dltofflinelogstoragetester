/**
 * @file Generator.cpp
 * @author Emanuel Ungureanu <emanuel.ungureanu@gmail.com>
 * @date 2018-10-28
 * 
 * @copyright Copyright (c) 2018 Emanuel Ungureanu. All rights reserved.
 * @license This project is released under the MIT License
 */
#include "Generator.hpp"

#include <iostream>

extern "C"
{
	#include <sys/types.h>
	#include <sys/wait.h>
	#include <unistd.h>
	#include <errno.h>
}

Generator::Generator()
{
}

void Generator::run()
{
	pid_t curPid = getpid();
	std::cout << "Generator pid: " << curPid << std::endl;
	sleep(5);
}