/**
 * @file Constants.hpp
 * @author Emanuel Ungureanu <emanuel.ungureanu@gmail.com>
 * @date 2018-10-29
 * 
 * @copyright Copyright (c) 2018 Emanuel Ungureanu. All rights reserved.
 * @license This project is released under the MIT License
 */
#pragma once

#include <cstddef>
#include <cstdint>


//#define TEST


namespace dlt
{
    namespace constants
    {
        static constexpr size_t DltIdSize = 4;

        namespace header_flags
        {
            static constexpr uint8_t USE_EXTENDED_HEADER = 1 << 0; // use Extended Header
            static constexpr uint8_t MSB_FIRST = 1 << 1; // MSB First
            static constexpr uint8_t WITH_ECU_ID = 1 << 2;
            static constexpr uint8_t WITH_SESSION_ID = 1 << 3;
            static constexpr uint8_t WITH_TIMESTAMP = 1 << 4;
            static constexpr uint8_t VERSION = (1<<5) | (1<<6) | (1<<7);
        };

        namespace extended_header
        {
            static constexpr uint8_t VerboseMask = 0x01;
            static constexpr uint8_t TypeMask = 0x0e;
            static constexpr uint8_t TypeShift = 0x01;
            static constexpr uint8_t SubtypeMask = 0xf0;
            static constexpr uint8_t SubtypeShift = 0x04;
        };

        namespace payload
        {
            namespace argument_type
            {
                static constexpr uint16_t Tyle = 0x0000000f; /**< Length of standard data: 1 = 8bit, 2 = 16bit, 3 = 32 bit, 4 = 64 bit, 5 = 128 bit */
                static constexpr uint16_t Bool = 0x00000010; /**< Boolean data */
                static constexpr uint16_t SignedInt = 0x00000020; /**< Signed integer data */
                static constexpr uint16_t UnsignedInt = 0x00000040; /**< Unsigned integer data */
                static constexpr uint16_t Float = 0x00000080; /**< Float data */
                static constexpr uint16_t Aray = 0x00000100; /**< Array of standard types */
                static constexpr uint16_t String = 0x00000200; /**< String */
                static constexpr uint16_t RawData = 0x00000400; /**< Raw data */
                static constexpr uint16_t VariableInfo = 0x00000800; /**< Set, if additional information to a variable is available */
                static constexpr uint16_t FixedPoint = 0x00001000; /**< Set, if quantization and offset are added */
                static constexpr uint16_t TraceInfo = 0x00002000; /**< Set, if additional trace information is added */
                static constexpr uint16_t Structure = 0x00004000; /**< Struct */
                static constexpr uint32_t StringCoding = 0x00038000; /**< coding of the type string: 0 = ASCII, 1 = UTF-8 */
            }

            namespace type_length
            {
                static constexpr uint8_t Bits_8 = 0x00000001;
                static constexpr uint8_t Bits_16 = 0x00000002;
                static constexpr uint8_t Bits_32 = 0x00000003;
                static constexpr uint8_t Bits_64 = 0x00000004;
                static constexpr uint8_t Bits_128 = 0x00000005;
            }

            namespace string_coding
            {
                static constexpr size_t Ascii = 0x00000000;
                static constexpr size_t Utf8 = 0x00008000;
                static constexpr size_t Hex = 0x00010000;
                static constexpr size_t Binary = 0x00018000;
            }
        };
    }
}
