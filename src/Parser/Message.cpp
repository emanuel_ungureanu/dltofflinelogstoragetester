/**
 * @file Message.cpp
 * @author Emanuel Ungureanu <emanuel.ungureanu@gmail.com>
 * @date 2018-10-29
 * 
 * @copyright Copyright (c) 2018 Emanuel Ungureanu. All rights reserved.
 * @license This project is released under the MIT License
 */
#include "Message.hpp"

namespace dlt
{
	namespace header
	{
		///////////////////////////////////////////
		//  Standard Header Flags
		///////////////////////////////////////////
		StandardHeaderFlags::StandardHeaderFlags(uint8_t flags) :
			mFlags(flags)
		{
		}

		uint8_t StandardHeaderFlags::getFlags() const
		{
			return mFlags;
		}

		bool StandardHeaderFlags::isHeaderExtended() const
		{
			using namespace dlt::constants::header_flags;
			return hasFlag(USE_EXTENDED_HEADER);
		}

		bool StandardHeaderFlags::isBigEndian() const
		{
			using namespace dlt::constants::header_flags;
			return hasFlag(MSB_FIRST);
		}

		bool StandardHeaderFlags::hasEcuId() const
		{
			using namespace dlt::constants::header_flags;
			return hasFlag(WITH_ECU_ID);
		}

		bool StandardHeaderFlags::hasSessionId() const
		{
			using namespace dlt::constants::header_flags;
			return hasFlag(WITH_SESSION_ID);
		}

		bool StandardHeaderFlags::hasTimestamp() const
		{

			using namespace dlt::constants::header_flags;
			return hasFlag(WITH_TIMESTAMP);
		}

		uint8_t StandardHeaderFlags::getVersion() const
		{
			using namespace dlt::constants::header_flags;
			return mFlags & VERSION;
		}

		bool StandardHeaderFlags::hasFlag(const uint8_t flag) const
		{
			return utils::isFlagInMask<size_t>(mFlags, flag);
		}






		///////////////////////////////////////////
		//  Standard Header
		///////////////////////////////////////////

		size_t StandardHeader::getSize() const
		{
			size_t size = sizeof(StandardHeader);

			if (HeaderFlags.hasEcuId() == true)
			{
				size += dlt::constants::DltIdSize;
			}

			if (HeaderFlags.hasSessionId() == true)
			{
				size += dlt::constants::DltIdSize;
			}

			if (HeaderFlags.hasTimestamp() == true)
			{
				size += sizeof(uint32_t);
			}

			return size;
		}

        bool StandardHeader::hasExtendedHeader() const
        {
            return HeaderFlags.isHeaderExtended();
        }



		const char* StandardHeader::getEcuId() const
		{
			return mOptionalMembers;
		}

		const char* StandardHeader::getSessionId() const
		{
			size_t offset = 0;
			if (HeaderFlags.hasEcuId() == true)
			{
				offset += dlt::constants::DltIdSize;
			}

			return mOptionalMembers + offset;
		}

		uint32_t StandardHeader::getTimestamp() const
		{
			size_t offset = 0;

			if (HeaderFlags.hasEcuId() == true)
			{
				offset += dlt::constants::DltIdSize;
			}

			if (HeaderFlags.hasSessionId() == true)
			{
				offset += dlt::constants::DltIdSize;
			}

			return *(uint32_t*)(mOptionalMembers + offset);
		}

		bool StandardHeader::hasEcuId() const
		{
			return HeaderFlags.hasEcuId();
		}

		bool StandardHeader::hasSessionId() const
		{
			return HeaderFlags.hasSessionId();
		}

		bool StandardHeader::hasTimestamp() const
		{
			return HeaderFlags.hasTimestamp();
		}

        uint16_t StandardHeader::getMessageLength() const
        {
            return utils::swapBytes(mLength);
        }







		///////////////////////////////////////////
		//  Message Info Flags
		///////////////////////////////////////////
		bool MessageInfoFlags::isVerbose() const
		{
			return utils::isFlagInMask<uint8_t>(0x1, Flags);
		}

		MessageType MessageInfoFlags::getMessageType() const
		{
			uint8_t messageTypeMask = (0xF - 2); // negate the verbose flag
			MessageType messageType = static_cast<MessageType>(Flags & messageTypeMask);
			return messageType;
		}
	}










    namespace payload
    {
        ///////////////////////////////////////////
        //  Payload
        ///////////////////////////////////////////

        bool Payload::getArgumentBool(const size_t offset) const
        {
            const char* boolPtr = mNonStaticData + offset;
            bool value = ((const bool*)( boolPtr ))[0];
            return value;
        }

        std::string Payload::getArgumentString(const size_t offset) const
        {
            static const size_t lengthSize = sizeof(uint16_t);
            const char* lengthPtr = mNonStaticData + offset;
            const char* dataPtr = mNonStaticData + offset + lengthSize;

            uint16_t length = ( ( uint16_t* ) lengthPtr )[0];
            length -= 1; // includes /0
            std::string value(dataPtr, length);
            return value;
        }


        std::string Payload::getArgumentRawData(const size_t offset) const
        {
            static const size_t lengthSize = sizeof(uint16_t);
            const char* lengthPtr = mNonStaticData + offset;
            const char* dataPtr = mNonStaticData + offset + lengthSize;

            uint16_t length = ( ( uint16_t* ) lengthPtr )[0];
            std::string value(dataPtr, length);
            return value;
        }
    }








	///////////////////////////////////////////
	//  Message
	///////////////////////////////////////////

	void Message::init(const char* messageBuffer)
	{
		mMessageBuffer = messageBuffer;
		mHeaderStorage = (const header::StorageHeader*)(mMessageBuffer);

		mHeaderStandardOffset = sizeof(header::StorageHeader);
		mHeaderStandard = (const header::StandardHeader*)(mMessageBuffer + mHeaderStandardOffset);
		mHeaderStandardSize = getStandardHeader()->getSize();

		mHeaderExtendedOffset = mHeaderStandardOffset + mHeaderStandardSize;
		mHeaderExtended = (const header::ExtendedHeader*)(mMessageBuffer + mHeaderExtendedOffset);

		mPayloadOffset = mHeaderExtendedOffset + sizeof(header::ExtendedHeader);
        mPayload = ( const payload::Payload* ) ( mMessageBuffer + mPayloadOffset );

	}

	void Message::clean()
	{
		mMessageBuffer = nullptr;
		mHeaderStorage = nullptr;
		mHeaderStandardOffset = 0;
		mHeaderStandardSize = 0;
		mHeaderStandard = nullptr;
		mHeaderExtendedOffset = 0;
		mPayloadOffset = 0;
		mHeaderExtended = nullptr;
	}

	bool Message::isValid() const
	{
		return mMessageBuffer == nullptr;
	}

	bool Message::isValid(size_t messageSize) const
	{
		size_t storedMessageSize = sizeof(header::StorageHeader) + messageSize;
		return getStandardHeader()->getMessageLength() == storedMessageSize;
	}

	// Mapped data structures
	const header::StorageHeader* Message::getStorageHeader() const
	{
        return mHeaderStorage;
	}

	const header::StandardHeader* Message::getStandardHeader() const
	{
        return mHeaderStandard;
	}

	const header::ExtendedHeader* Message::getExtendedHeader() const
	{
        return mHeaderExtended;
	}

    const payload::Payload* Message::getPayload() const
    {
        return mPayload;
    }

    size_t Message::getPayloadSize() const
    {
        return mHeaderStandard->getMessageLength() - mHeaderStandard->getSize() - sizeof(header::ExtendedHeader);
    }
}
