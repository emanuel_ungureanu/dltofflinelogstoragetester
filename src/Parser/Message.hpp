/**
 * @file Message.hpp
 * @author Emanuel Ungureanu <emanuel.ungureanu@gmail.com>
 * @date 2018-10-29
 * 
 * @copyright Copyright (c) 2018 Emanuel Ungureanu. All rights reserved.
 * @license This project is released under the MIT License
 */
#pragma once

#include <array>
#include <vector>
#include <iostream>

#include "Platform.hpp"
#include "Constants.hpp"
#include "Utils.hpp"

// Disable warning C4200 for zero sized array
#ifdef WIN
#pragma warning(push)
#pragma warning(disable : 4200)
#endif
namespace dlt
{
	namespace header
	{
		using DltId = char[constants::DltIdSize];


		/*
		 * Standard Header Timestamp
		 */
		PACK(struct Timestamp
		{
			uint32_t Seconds;
			uint32_t Microseconds;
		});



		/*
		 *  Storage Header
		 */
		PACK(class StorageHeader
		{
		public:
			DltId DltMessagePattern;
			Timestamp MessageTimestamp;
			DltId EcuId;
		});



		/*
		 * Standard header flags
		 */
		PACK(class StandardHeaderFlags
		{
		public:
			StandardHeaderFlags(uint8_t flags);

			uint8_t getFlags() const;

			bool isHeaderExtended() const;
			bool isBigEndian() const;
			bool hasEcuId() const;
			bool hasSessionId() const;
			bool hasTimestamp() const;
			uint8_t getVersion() const;

		private:
			bool hasFlag(const uint8_t flag) const;

		private:
			uint8_t mFlags = 0;
		});



		/*
		 * Standard Header
		 */
		PACK(class StandardHeader
		{
		public:
			StandardHeader(StandardHeader &other) = delete;
			StandardHeader(StandardHeader &&other) = delete;

			StandardHeader& operator = (StandardHeader &other) = delete;
			StandardHeader& operator = (StandardHeader &&other) = delete;

			size_t getSize() const;
            bool hasExtendedHeader() const;
			const char* getEcuId() const;
			const char* getSessionId() const;
			uint32_t getTimestamp() const;

			bool hasEcuId() const;
			bool hasSessionId() const;
			bool hasTimestamp() const;
            uint16_t getMessageLength() const;

		public:
			StandardHeaderFlags HeaderFlags;
			uint8_t MessageCounter;

        private:
			uint16_t mLength; // without Storage header, in Big Endian Format

		private:
			const char mOptionalMembers[0];
		});



		enum class MessageType : uint8_t
		{
			LOG = 0,
			APPLICATION_TRACE,
			NETWORK_TRACE,
			CONTROL,
			UNKOWN
		};



		enum class MessageLogInfo : uint8_t
		{
			FATAL = 1,
			ERROR,
			WARN,
			INFO,
			DEBUG,
			VERBOSE,
			UNKOWN
		};



		enum class MessageTraceInfo : uint8_t
		{
			VARIABLE = 1,
			FUNCTION_IN,
			FUNCTION_OUT,
			STATE,
			VFB,
			UNKOWN
		};



		enum class MessageBusInfo : uint8_t
		{
			IPC = 1,
			CAN,
			FLEXRAY,
			MOST,
			UNKOWN
		};



		enum class MessageControlInfo : uint8_t
		{
			REQUEST = 1,
			RESPONSE,
			CONTROL_TIME,
			UNKOWN
		};


		/*
		 * Message Info Flags
		 */
		PACK(class MessageInfoFlags
		{
		public:
			bool isVerbose() const;
			MessageType getMessageType() const;

			template<typename T>
			T getMessageSubType() const
			{
				uint8_t subType = (Flags & 0xF0) >> 4;
				return static_cast<T>(subType);
			}

			uint8_t Flags;
		});






		/*
		 * Extended header
	 	 */
		PACK(class ExtendedHeader
		{
		public:
			MessageInfoFlags MessageInfo;
			uint8_t PayloadArgumentCount;
			const char ApplicationId[dlt::constants::DltIdSize];
			const char ContextId[dlt::constants::DltIdSize];
		});
	}

    namespace payload
    {
        /*
         *   Payload
         */
        PACK(class Payload
        {
        public:
            uint32_t MessageId; // static data

            bool getArgumentBool(const size_t offset) const;

            template<typename T>
            inline T getArgumentData(const size_t offset) const;

            std::string getArgumentString(const size_t offset) const;

            std::string getArgumentRawData(const size_t offset) const;

        private:
            const char mNonStaticData[0]; // non static data
        });




            template<typename T>
            inline T Payload::getArgumentData(const size_t offset) const
            {
                return  ( ( T* ) ( mNonStaticData + offset ) )[0];
            }
    }



	class Message
	{
	public:
		void init(const char* messageBuffer);
		void clean();
		bool isValid() const;
		bool isValid(size_t messageSize) const;

		// Mapped data structures
		const header::StorageHeader* getStorageHeader() const;
		const header::StandardHeader* getStandardHeader() const;
		const header::ExtendedHeader* getExtendedHeader() const;
        const payload::Payload* getPayload() const;
        size_t getPayloadSize() const;

	private:
		// Raw message buffer
		const char *mMessageBuffer = nullptr;

		// meta data
		const header::StorageHeader *mHeaderStorage = nullptr;

		size_t mHeaderStandardOffset = 0;
		size_t mHeaderStandardSize = 0;
		const header::StandardHeader *mHeaderStandard = nullptr;

		size_t mHeaderExtendedOffset = 0;
		const header::ExtendedHeader *mHeaderExtended = nullptr;

		size_t mPayloadOffset = 0;
        const payload::Payload *mPayload = nullptr;
	};
}

// Restore warning C4200
#ifdef WIN
#pragma warning(pop)
#endif