/**
 * @file MessageParser.cpp
 * @author Emanuel Ungureanu <emanuel.ungureanu@gmail.com>
 * @date 2018-10-29
 * 
 * @copyright Copyright (c) 2018 Emanuel Ungureanu. All rights reserved.
 * @license This project is released under the MIT License
 */
#include "MessageParser.hpp"

#include "../Utils.hpp"

#include <cstring>
#include <fstream>

namespace dlt
{
	namespace file
	{
		///////////////////////////////////////////
		//  Message Index List
		///////////////////////////////////////////
	#pragma region MessageIndex
		MessageFileOffsetVector::MessageFileOffsetVector(std::unique_ptr<std::vector<size_t>> &offsets, const size_t fileSize) :
			mOffsets(std::move(offsets)), 
            mMaxOffset(fileSize)
		{
		}



		size_t MessageFileOffsetVector::getMessageSize(size_t index) const
		{
			const size_t offset = at(index);
            size_t nextOffset = mMaxOffset; // last offset

			// Is not message ?
            const size_t lastIndex = getSize() - 1;
			if (index < lastIndex)
			{
                nextOffset = at(index + 1);
			}

            const size_t size = nextOffset - offset;
            return size;
		}



		size_t MessageFileOffsetVector::getSize() const
		{
			return mOffsets->size();
		}



		const size_t& MessageFileOffsetVector::operator[](size_t index) const
		{
			return (*mOffsets)[index];
		}


        const size_t& MessageFileOffsetVector::at(size_t index) const
        {
            return (*mOffsets)[index];
        }









		///////////////////////////////////////////
		//  Message Index List Builder
		///////////////////////////////////////////

        class MessageIndexListBuilderImpl
        {
        public:
            MessageIndexListBuilderImpl(std::istream &inputStream);
            std::unique_ptr<MessageFileOffsetVector> getMessageIndexList();

            bool fillBuff(std::istream &input);
            void partialTagBufferFinish();
            void partialTagBufferSearch();
            void searchWindow();
            void partialTagBufferInit();

        private:
            size_t mReadSize = 0;
            size_t mSearchWindowOffset = 0;
            size_t mFileSize = 0;
            size_t mReadByteCount = 0;
            std::unique_ptr<std::vector<size_t>> mIndexList;

            struct ReadBuffer
            {
                static constexpr size_t sBufSize = 300;
                std::array<char, sBufSize> Buf;
                char* BufPtr = &Buf[0];

                void readFirstThree(char *destination);
                void read(char *destination, size_t offset, size_t size);
            } mBufferRead;

            struct PartialTagBuffer
            {
                static constexpr size_t sSize = 6;
                std::array<char, sSize> Buf;
                char* BufPtr = &Buf[0];
            } mBufPartialTag;

            static struct DltTag
            {
                static constexpr size_t Size = 4;
                static constexpr std::array<const char, Size> Tag = { 'D', 'L' , 'T' , 0x01 };
                static constexpr const char* Ptr = &Tag[0];
            } sDltTag;

        };

        constexpr std::array<const char, MessageIndexListBuilderImpl::DltTag::Size> MessageIndexListBuilderImpl::DltTag::Tag; // = { 'D', 'L' , 'T' , 0x01 };


		constexpr size_t MessageIndexListBuilderImpl::ReadBuffer::sBufSize;



		MessageIndexListBuilderImpl::MessageIndexListBuilderImpl(std::istream &inputStream) :
			mIndexList(new std::vector<size_t>())
		{
			using namespace std;

			mFileSize = utils::getStreamSize(inputStream);

			memset((void*)mBufPartialTag.BufPtr, 0, mBufPartialTag.sSize);

			while (inputStream && fillBuff(inputStream))
			{
				partialTagBufferFinish();
				partialTagBufferSearch();
				searchWindow();
				partialTagBufferInit();
				mSearchWindowOffset += mReadSize;
			}
		}



		std::unique_ptr<MessageFileOffsetVector> MessageIndexListBuilderImpl::getMessageIndexList()
		{
			return std::make_unique<MessageFileOffsetVector>(mIndexList, mFileSize);
		}



		bool MessageIndexListBuilderImpl::fillBuff(std::istream &dltInputStream)
		{
			size_t bytesToRead = std::min(mFileSize - mReadByteCount, mBufferRead.sBufSize);

			dltInputStream.read(mBufferRead.BufPtr, bytesToRead);
			mReadSize = dltInputStream.gcount();
			mReadByteCount += mReadSize;
			return mReadSize >= sDltTag.Size;
		}



		void MessageIndexListBuilderImpl::partialTagBufferFinish()
		{
			// copy first 3 chars to partial tag Buffer
			mBufferRead.readFirstThree(mBufPartialTag.BufPtr + 3);
		}



		void MessageIndexListBuilderImpl::partialTagBufferSearch()
		{
			// search for tags split over consecutive search windows
			char *result(mBufPartialTag.BufPtr);

			const size_t partialTagWindowOffset = mSearchWindowOffset - 3;

			const size_t searchSize = mReadSize - sDltTag.Size - 1;

			for (size_t bufPos = 0; bufPos < searchSize; ++bufPos)
			{
				char *searchStr = mBufPartialTag.BufPtr + bufPos;
				if (memcmp(searchStr, sDltTag.Ptr, sDltTag.Size) == 0)
				{
					const size_t messageOffset = partialTagWindowOffset + bufPos;
					mIndexList->push_back(messageOffset);
					bufPos += sDltTag.Size;
					break;
				}
			}
		}



		void MessageIndexListBuilderImpl::searchWindow()
		{
			// search in the current window
			char *result(mBufferRead.BufPtr);

			static constexpr size_t searchSize = (MessageIndexListBuilderImpl::ReadBuffer::sBufSize - sDltTag.Size - 1);

			for (size_t bufPos = 0; bufPos < mReadSize; ++bufPos)
			{
				char *searchStr = mBufferRead.BufPtr + bufPos;
				if (memcmp(searchStr, sDltTag.Ptr, sDltTag.Size) == 0)
				{

					const size_t messageOffset = mSearchWindowOffset + bufPos;
					mIndexList->push_back(messageOffset);
				}
			}
		}



		void MessageIndexListBuilderImpl::partialTagBufferInit()
		{
			memset(mBufPartialTag.BufPtr, 0, mBufPartialTag.sSize);

			// copy last 3 chars to partial tag buffer
			// in order to catch tags split between search windows
			const size_t offset(mReadSize - 3);
			const size_t size(3);
			mBufferRead.read(mBufPartialTag.BufPtr, offset, size);
		}



		void MessageIndexListBuilderImpl::ReadBuffer::readFirstThree(char *destination)
		{
			const size_t offset(0), size(3);
			read(destination, offset, size);
		}



		void MessageIndexListBuilderImpl::ReadBuffer::read(char *destination, size_t offset, size_t size)
		{
			const char* copyPtr = &BufPtr[offset];
			memcpy((void*)destination, copyPtr, size);
		}




		std::unique_ptr<MessageFileOffsetVector> MessageIndexListBuilder::createMessageIndexList(const std::string &file)
		{
			std::ifstream stream(file, std::ios::binary);
			MessageIndexListBuilderImpl indexBuilder(stream);
			return indexBuilder.getMessageIndexList();
		}





		///////////////////////////////////////////
		//  Message Stream
		///////////////////////////////////////////
		MessageStream::MessageStream(const std::string &file) :
			mMessages(std::ifstream(file, std::ios::binary)),
			mOffsets(std::move(MessageIndexListBuilder::createMessageIndexList(file))),
			mCurrentIndex(0)
		{
			reset();
		}



		size_t MessageStream::getSize()
		{
			return utils::getStreamSize(mMessages);
		}



		void MessageStream::reset()
		{
			mMessages.clear();
			mMessages.seekg(0, std::ios::beg);
			mCurrentIndex = 0;
		}



		size_t MessageStream::getNextMessageSize() const
		{
			return getMessageSize(mCurrentIndex);
		}


		
		size_t MessageStream::getMessageSize(size_t index) const
		{
			return mOffsets->getMessageSize(index);
		}



		size_t MessageStream::getMessageCount() const
		{
			if (mOffsets)
			{
				return mOffsets->getSize();
			}
			else
			{
				return 0;
			}
		}



		std::vector<char> MessageStream::read()
		{
			size_t messageSize = getNextMessageSize();
			std::vector<char> messageBuffer(messageSize, 0);
			read(messageBuffer);
			return messageBuffer;
		}



		size_t MessageStream::read(std::vector<char> &messageBuffer)
		{
			// Workaround in order ro have both readMessage and read methods working
			//seek(mCurrentMessageIndex);

			size_t messageSize = mOffsets->getMessageSize(mCurrentIndex);
			mMessages.read( &messageBuffer[0], messageSize);
			++mCurrentIndex;
			return messageSize;
		}



		void MessageStream::seek(size_t messageIndex)
		{
			const size_t& messageOffset = (*mOffsets)[messageIndex];
			mMessages.seekg(messageOffset);
			mCurrentIndex = messageIndex;
		}



		size_t MessageStream::read(std::vector<char> &destination, size_t length)
		{
			mMessages.read(&destination[0], length);
			return mMessages.gcount();
		}
	}
}

