/**
 * @file MessageParser.hpp
 * @author Emanuel Ungureanu <emanuel.ungureanu@gmail.com>
 * @date 2018-10-29
 * 
 * @copyright Copyright (c) 2018 Emanuel Ungureanu. All rights reserved.
 * @license This project is released under the MIT License
 */
#pragma once


#include "../Message.hpp"

#include <iostream>
#include <vector>
#include <memory>
#include <fstream>
#include <sstream>

#include <exception>
#include <sstream>

#include <type_traits>


namespace dlt
{
    namespace file
    {
        class MessageFileOffsetVector
        {
        public:
            MessageFileOffsetVector(std::unique_ptr<std::vector<size_t>> &indexes, const size_t maxOffset);
            size_t getMessageSize(size_t index) const;
			size_t getSize() const;

			// get message offset
			const size_t& operator[](size_t index) const;

			// get message offset
            const size_t& at(size_t index) const;

        private:
            std::unique_ptr<std::vector<size_t>> mOffsets;
            size_t mMaxOffset;
        };



        class MessageIndexListBuilder
        {
        public:
			// Create a message index list from given dlt file stream
            static std::unique_ptr<MessageFileOffsetVector> createMessageIndexList(const std::string &file);
        };



        class MessageStream
        {
        public:
            MessageStream(const std::string &path);
			virtual ~MessageStream() {}

			// Returns the file size
			size_t getSize();

            void reset();

			// Returns the size of the next message in the stream
			size_t getNextMessageSize() const;

			// Returns the size of the message at index
			size_t getMessageSize(size_t index) const;

			// Returns the number of messages in this stream
			size_t getMessageCount() const;

			// Reads the next message in the stream and returns it as std::vector
			std::vector<char> read();

			// Buffer needs to be large enough to hold the message
			size_t read(std::vector<char> &messageBuffer);


            // Sets the message at given index as next message to be read
			void seek(size_t messageIndex);

		private:
			size_t read(std::vector<char> &destination, size_t length);

        private:
            std::ifstream mMessages;
            std::unique_ptr<MessageFileOffsetVector> mOffsets;
            size_t mCurrentIndex;
        };
    }
}
