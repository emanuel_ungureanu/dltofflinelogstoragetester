/**
 * @file Platform.hpp
 * @author Emanuel Ungureanu <emanuel.ungureanu@gmail.com>
 * @date 2018-10-29
 * 
 * @copyright Copyright (c) 2018 Emanuel Ungureanu. All rights reserved.
 * @license This project is released under the MIT License
 */
#pragma once

#ifdef _MSC_VER
#define WIN _MSC_VER
#endif

#if defined (WIN)
#define PACK( ... ) __pragma( pack(push, 8) ) __VA_ARGS__ __pragma( pack(pop) )
#else
#define PACK(__Declaration__)  \
    __Declaration__ \
    __attribute__((aligned(1),packed))
#endif