/**
 * @file Stopwatch.cpp
 * @author Emanuel Ungureanu <emanuel.ungureanu@gmail.com>
 * @date 2018-10-29
 * 
 * @copyright Copyright (c) 2018 Emanuel Ungureanu. All rights reserved.
 * @license This project is released under the MIT License
 */
#include "Stopwatch.hpp"

Stopwatch::Stopwatch()
{
}

Stopwatch::Stopwatch(bool start)
{
    if (start == true)
    {
        Start();
    }
}


void Stopwatch::Start()
{
    mBeginClock = std::chrono::system_clock::now();
}

void Stopwatch::Stop()
{
    mEndClock = std::chrono::system_clock::now();
}

size_t Stopwatch::GetElapsedTime()
{
    mDuration = std::chrono::duration_cast<std::chrono::milliseconds>(mEndClock - mBeginClock);
    return mDuration.count();
}