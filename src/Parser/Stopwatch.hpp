/**
 * @file Stopwatch.hpp
 * @author Emanuel Ungureanu <emanuel.ungureanu@gmail.com>
 * @date 2018-10-29
 * 
 * @copyright Copyright (c) 2018 Emanuel Ungureanu. All rights reserved.
 * @license This project is released under the MIT License
 */
#pragma once

#include <chrono>

class Stopwatch
{
public:
    Stopwatch();
    Stopwatch(bool start);

    void Start();
    void Stop();
    size_t GetElapsedTime();

private:
    std::chrono::time_point<std::chrono::system_clock> mBeginClock;
    std::chrono::time_point<std::chrono::system_clock> mEndClock;
    std::chrono::milliseconds mDuration;
};