/**
 * @file Utils.hpp
 * @author Emanuel Ungureanu <emanuel.ungureanu@gmail.com>
 * @date 2018-10-29
 * 
 * @copyright Copyright (c) 2018 Emanuel Ungureanu. All rights reserved.
 * @license This project is released under the MIT License
 */
#pragma once

#include <cstdint>

// For #define TEST
#include "Constants.hpp"

#include <iostream>

namespace utils
{
    template<typename T>
    T isFlagInMask(const T value, const T flag);


    template<typename T>
    T swapBytes(T data);


    template<>
    uint16_t swapBytes(uint16_t data);


    template<>
    uint32_t swapBytes(uint32_t data);


    inline size_t getStreamSize(std::istream &stream);
}


namespace utils
{
    template<typename T>
    T isFlagInMask(const T value, const T flag)
    {
        return value & flag;
    }



    template<typename T>
    inline T swapBytes(T data)
    {
        return data;
    }



    template<>
    inline uint16_t swapBytes(uint16_t data)
    {
        uint16_t firstByte = (data >> 8);
        uint16_t secondByte = (data << 8) & 0xFF00;

        uint16_t result = secondByte | firstByte;
        return result;
    }



    template<>
    inline uint32_t swapBytes(uint32_t data)
    {
        uint32_t firstByte = (data >> 24);
        uint32_t secondByte = (data >> 8) & 0xFF00;
        uint32_t firstHalf = firstByte | secondByte;

        uint32_t thirdByte = (data << 8) & 0x00FF0000;
        uint32_t fourthByte = (data << 24) & 0xFF000000;
        uint32_t secondHalf = fourthByte | thirdByte;

        uint32_t result = secondHalf | firstHalf;
        return result;
    }



    inline size_t getStreamSize(std::istream &stream)
    {
        size_t position = (size_t)stream.tellg();

        stream.seekg(0, stream.beg);
        stream.seekg(0, stream.end);
        size_t streamSize = (size_t)stream.tellg();
        stream.seekg(position, stream.beg);

        return streamSize;
    }
}
