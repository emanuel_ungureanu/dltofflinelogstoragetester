/**
 * @file main.cpp
 * @author Emanuel Ungureanu <emanuel.ungureanu@gmail.com>
 * @date 2018-10-28
 * 
 * @copyright Copyright (c) 2018 Emanuel Ungureanu. All rights reserved.
 * @license This project is released under the MIT License
 */

#include <iostream>
#include <string>
#include <sstream>

#include <cstdlib>
#include <cstring>


extern "C"
{
	#include <sys/types.h>
	#include <sys/wait.h>
	#include <unistd.h>
	#include <errno.h>
}


#include "Parser/Constants.hpp"
#include "Parser/Parser/MessageParser.hpp"
#include "Parser/Utils.hpp"
#include "Parser/Stopwatch.hpp"


#include "ConfigGenerator.hpp"
#include "Generator/Generator.hpp"


////////////////////////////
//  MASTER
////////////////////////////


// Returns true if parent
// false otherwise
bool spawnLogGenerators(std::vector<pid_t> &generatorPids, size_t generatorCount)
{
	using namespace std;
	for(int i = 0; i < generatorCount; ++i)
	{
		pid_t pId = fork();
		if(pId == 0)
		{
			return true;
		}
		if(pId == -1)
		{
			cerr << "Error forking" << endl;
			exit(-1);
		}

		generatorPids.push_back(pId);
	}

	return false;
}

bool spawnDltDaemon(pid_t &dltDaemonPid, std::string dltDaemonPath , std::string confPath)
{
	using namespace std;

	dltDaemonPid = fork();
	if(dltDaemonPid == 0)
	{
		// transform into dlt-daemon
		char *const argv[] = { strdup(dltDaemonPath.c_str()), strdup("-c"), strdup(confPath.c_str()), nullptr };
		char *const envp[] = { nullptr };

		cout << "Spawning '" << dltDaemonPath << "'. With args: " << endl;
		cout << "'" << argv[0] <<  "', '" << argv[1] << "', '" << argv[2] << "'" << endl;

		close(STDIN_FILENO);
		//close(STDOUT_FILENO);
		//close(STDERR_FILENO);


		int retVal = execve(dltDaemonPath.c_str(), argv, envp);
		cout << "Error execve dltdaemon: " << retVal << ". Errno: " << errno << ". Description: " << strerror(errno)  <<  endl;
		exit(-1);
	}

	cout << "Dlt daemon pid: " << dltDaemonPid << endl;
}

void waitLogGenerators(const std::vector<pid_t> &generatorPids)
{
	using namespace std;

	size_t generatorRunning = generatorPids.size();
	int activeGeneratorCount = generatorPids.size();

	cout << "Waiting generators to finish" << endl;

	while(activeGeneratorCount > 0)
	{
		for(int i = 0; i <generatorPids.size(); ++i)
		{
			int status;
			pid_t waitedPid = waitpid(generatorPids[i], &status, WNOHANG);
			if(waitedPid > 0)
			{
				--activeGeneratorCount;
				cout << "Detected exit of: " << waitedPid << endl;
			}
		}
		sleep(1);
	}

	cout << "All log generators exited" << endl;
}

void waitDaemon(const pid_t daemonPid)
{
	std::cout << "Waiting for daemon to exit" << std::endl;
	int status = 0;
	pid_t waitPid = waitpid(daemonPid, &status, 0);
	std::cout << "Dlt daemon finished" << std::endl;
}

class AppConfig
{
public:
    AppConfig()
    {
        CurrentWorkingDirectory = GetCurrentWorkingDirectory();
        WorkDir = GetWorkDir();
        BinaryPath = GetBinaryPath();
        DltConfTemplatePath = GetDltConfTemplatePath();
        DltConfPath = GetDltConfPath();
        DltLogstoragePath = GetDltLogstoragePath();
        DltFiltersPath = GetDltFiltersPath();
    }

public:
    std::string CurrentWorkingDirectory;
    std::string WorkDir;
    std::string BinaryPath;
    std::string DltConfTemplatePath;
    std::string DltConfPath;
    std::string DltLogstoragePath;
    std::string DltFiltersPath;

private:

    std::string GetCurrentWorkingDirectory()
    {
        std::vector<char> cwdBuf(1000, 0);
        getcwd(&cwdBuf[0], cwdBuf.size());

        const size_t cwdLength = strlen(&cwdBuf[0]);

        return std::string(&cwdBuf[0], cwdLength);
    }

    std::string GetWorkDir()
    {
        std::stringstream builder;
        builder << CurrentWorkingDirectory << "/bin/tmp";
        return builder.str();
    }

    std::string GetBinaryPath()
    {
        std::stringstream builder;
        builder << CurrentWorkingDirectory << "/bin/dlt-daemon";
        return builder.str();
    }

    std::string GetDltConfTemplatePath()
    {
        std::stringstream builder;
        builder << CurrentWorkingDirectory << "/bin/dlt.conf.template";
        return builder.str();
    }

    std::string GetDltConfPath()
    {
        std::stringstream builder;
        builder << CurrentWorkingDirectory << "/bin/dlt.conf";
        return builder.str();
    }

    std::string GetDltLogstoragePath()
    {
        std::stringstream builder;
        builder << CurrentWorkingDirectory << "/logstorage";
        return builder.str();
    }

    std::string GetDltFiltersPath()
    {
        std::stringstream builder;
        builder << CurrentWorkingDirectory << "/logstorage/dlt_logstorage.conf";
        return builder.str();
    }
};



int main(int argc, char **argv)
{
	using namespace std;

	pid_t daemonPid;

    AppConfig config;

	ConfigGenerator configGenerator(config.DltConfTemplatePath, config.DltConfPath, config.WorkDir, config.DltLogstoragePath );
	configGenerator.generate();

	spawnDltDaemon(daemonPid, config.BinaryPath, config.DltConfPath );

	size_t generatorCount = 0;
	std::vector<pid_t> generatorPids;
	pid_t pId;

	// Spawn log generators
	if( spawnLogGenerators(generatorPids, generatorCount) )
	{
        Generator generator;
        generator.run();
		return 0;
	}


	// Wait for generators to finish
	waitLogGenerators(generatorPids);


	waitDaemon(daemonPid);
    
	return 0;
}